# HƯỚNG DẪN CÀI ĐẶT

## Yêu cầu hệ thống

- Framework: Flutter v3.3.0
- Ứng dụng AndroidStudio
- Ứng dụng VS Code
- Device model test: Pixel 5 API 28

## Run app
- Mở ứng dụng VS Code hoặc Android Studio

Chạy lệnh dưới đây:
```sh
flutter pub get # Chạy lệnh này để cài đặt package cần thiết để chạy ứng dụng
```
Chạy lệnh flutter run để chạy ứng dụng, hoặc sử dụng giao diện:
- Trên VS Code chọn Run/Start Debugging
- Trên Android Studio bấm vào biểu tượng chạy ứng dụng

## Build app
Chạy lệnh dưới đây để build file apk:
```sh
flutter build apk --release
```

## Liên kết tham khảo
- Link gitlab: https://gitlab.com/pekonsaysua/onemount-test

|                | Link                                                           |
| -------------- | -------------------------------------------------------------- |
| Flutter        | [https://docs.flutter.dev/get-started/install/windows]         |
| Android Studio | [https://docs.flutter.dev/get-started/editor?tab=androidstudio]|
