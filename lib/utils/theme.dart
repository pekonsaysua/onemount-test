import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';

Color primaryColor = const Color(0xffd82d1b);
Color whiteColor = const Color(0xffFFFFFF);
Color blackColor = const Color(0xff14193F);
Color greyColor = const Color(0xffA4ABAE);
Color blueColor = const Color(0xff53C1F9);
Color purpleColor = const Color(0xff5142E6);
Color redColor = const Color(0xffFF2566);
Color greenColor = const Color(0xff22B07D);
Color numberBackgroundColor = const Color(0xff1A1D2E);
Color lightBackgroundColor = const Color(0xffF6F8FB);
Color darkBackgroundColor = const Color(0xff020518);

TextStyle blackTextStyle = TextStyle(
  color: blackColor,
);

TextStyle whiteTextStyle = TextStyle(
  color: whiteColor,
);

TextStyle greyTextStyle = TextStyle(
  color: greyColor,
);

TextStyle blueTextStyle = TextStyle(
  color: blueColor,
);

TextStyle greenTextStyle = TextStyle(
  color: greenColor,
);

FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extraBold = FontWeight.w800;
FontWeight black = FontWeight.w900;

MaterialColor getMaterialColor(Color color) {
  final int red = color.red;
  final int green = color.green;
  final int blue = color.blue;

  final Map<int, Color> shades = {
    50: Color.fromRGBO(red, green, blue, .1),
    100: Color.fromRGBO(red, green, blue, .2),
    200: Color.fromRGBO(red, green, blue, .3),
    300: Color.fromRGBO(red, green, blue, .4),
    400: Color.fromRGBO(red, green, blue, .5),
    500: Color.fromRGBO(red, green, blue, .6),
    600: Color.fromRGBO(red, green, blue, .7),
    700: Color.fromRGBO(red, green, blue, .8),
    800: Color.fromRGBO(red, green, blue, .9),
    900: Color.fromRGBO(red, green, blue, 1),
  };

  return MaterialColor(color.value, shades);
}
