import 'package:ecommerce/utils/theme.dart';
import 'package:ecommerce/widgets/product_widget.dart';
import 'package:flutter/material.dart';

class DisplayPage extends StatefulWidget {
  const DisplayPage({super.key});

  @override
  State<DisplayPage> createState() => _DisplayPageState();
}

class _DisplayPageState extends State<DisplayPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: primaryColor,
      child: SingleChildScrollView(
        child: Column(
          children: [
            const ProductWidget(),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Divider(
                    height: 20,
                    thickness: 0.5,
                    indent: 20,
                    endIndent: 10,
                    color: whiteColor,
                  ),
                ),
                Text(
                  "Cùng nhiều gian hàng khác",
                  style: TextStyle(
                      color: whiteColor,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                ),
                Expanded(
                    child: Divider(
                  height: 20,
                  thickness: 0.5,
                  indent: 10,
                  endIndent: 20,
                  color: whiteColor,
                ))
              ],
            ),
            const ProductWidget(),
            const ProductWidget(),
          ],
        ),
      ),
    );
  }
}
