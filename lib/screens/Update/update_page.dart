import 'package:ecommerce/utils/theme.dart';
import 'package:flutter/material.dart';

class UpdatePage extends StatefulWidget {
  const UpdatePage({super.key});

  @override
  State<UpdatePage> createState() => _UpdatePageState();
}

class _UpdatePageState extends State<UpdatePage> {
  final int itemCount = 20;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: primaryColor,
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: itemCount,
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              title: Text('Contact ${(index + 1)}'),
              leading: const Icon(Icons.person_outline_rounded),
              trailing: const Icon(Icons.select_all_rounded),
              onTap: () {
                debugPrint('Contact ${(index + 1)}');
              },
            );
          }),
    );
  }
}
