import 'package:ecommerce/screens/display/display_page.dart';
import 'package:ecommerce/screens/update/update_page.dart';
import 'package:ecommerce/utils/theme.dart';
import 'package:ecommerce/widgets/custom_chip_widget.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ValueNotifier<int> _selectedIndex = ValueNotifier(0);
  ScrollController _scrollController = ScrollController(keepScrollOffset: true);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: _selectedIndex,
      builder: (context, value, child) {
        return Scaffold(
            extendBodyBehindAppBar: true,
            body: NestedScrollView(
              //floatHeaderSlivers: true,
              physics: const BouncingScrollPhysics(),
              controller: _scrollController,
              headerSliverBuilder: getHeader,
              body: getBody(),
            ),
            bottomNavigationBar: BottomNavigationBar(
              selectedItemColor: primaryColor,
              currentIndex: _selectedIndex.value,
              onTap: _onItemTapped,
              type: BottomNavigationBarType.fixed,
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  label: 'Trưng bày',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.notifications),
                  label: 'Cập nhật',
                ),
              ],
            ));
      },
    );
  }

  List<Widget> getHeader(BuildContext context, bool innerBoxIsScrolled) {
    return <Widget>[
      SliverAppBar(
        backgroundColor: primaryColor,
        elevation: 0,
        stretch: false,
        expandedHeight: 268,
        floating: true,
        pinned: true,
        snap: false,
        forceElevated: false,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          background: Image.asset(
            'assets/banner.jpg',
            fit: BoxFit.cover,
          ),
        ),
        bottom: const PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: Center(
            child: CustomChip(chipTextList: [
              "Đang tham gia (3)",
              "Chưa tham gia (2)",
              "Đã tham gia (4)"
            ]),
          ),
        ),
      ),
    ];
  }

  Widget getBody() {
    switch (_selectedIndex.value) {
      case 0:
        return const DisplayPage();
      case 1:
        return const UpdatePage();
      default:
        return const DisplayPage();
    }
  }

  void _onItemTapped(int index) {
    _selectedIndex.value = index;
  }
}
