import 'dart:ui';

import 'package:ecommerce/utils/theme.dart';
import 'package:flutter/material.dart';

class ProductWidget extends StatelessWidget {
  const ProductWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10),
      height: 270,
      width: double.infinity,
      decoration: const BoxDecoration(
          color: Color(0xffFFFFFF),
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Column(
        children: [buildHeader(), buildBody(), buildFooter()],
      ),
    );
  }

  Widget buildHeader() {
    return SizedBox(
      height: 40,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
              height: 40,
              padding: const EdgeInsets.symmetric(horizontal: 15),
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(10)),
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [Color(0xfffef5cb), Color(0xffFFFFFF)])),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(Icons.note_alt_outlined),
                  Text(
                    "Đang mở đăng ký",
                    style: TextStyle(
                        color: Color(0xffff6006), fontWeight: FontWeight.w500),
                  ),
                ],
              )),
          Container(
              margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
              height: 30,
              width: 120,
              decoration: const BoxDecoration(
                  color: Color(0xfffff2e2),
                  borderRadius: BorderRadius.all(Radius.circular(25))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(Icons.alarm),
                  Text("29/02 - 05/03",
                      style: TextStyle(fontWeight: FontWeight.w500)),
                ],
              ))
        ],
      ),
    );
  }

  Widget buildBody() {
    return Expanded(
      child: Container(
        height: 170,
        padding: const EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              child: Image.asset('assets/product.png'),
              width: 100,
            ),
            const SizedBox(
              width: 20,
            ),
            Expanded(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'Trưng bày Bia Budweiser tại kệ hàng tại cửa hàng',
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 21, height: 1.4),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  width: 250,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [Color(0xfffe5d05), Color(0xffe81d0a)])),
                  child: TextButton(
                    style: TextButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            vertical: 0, horizontal: 8)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Đăng ký ngay",
                          style: TextStyle(color: whiteColor),
                        ),
                        Wrap(
                          children: const [
                            Icon(
                              Icons.keyboard_arrow_right,
                              size: 35,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.keyboard_arrow_right,
                              size: 35,
                              color: Colors.white,
                            ),
                          ],
                        )
                      ],
                    ),
                    onPressed: () {},
                  ),
                )
              ],
            ))
          ],
        ),
      ),
    );
  }

  Widget buildFooter() {
    return Stack(children: [
      ClipRRect(
          borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          child: Image.asset(
            'assets/bottom.jpg',
            fit: BoxFit.fitHeight,
          )),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
              padding: const EdgeInsets.only(left: 68, top: 35),
              child: const Text(
                "Hoàn thành mọi nhiệm vụ",
                style: TextStyle(fontWeight: FontWeight.w600),
              )),
          Container(
            alignment: Alignment.centerRight,
            padding: const EdgeInsets.only(top: 5, right: 10),
            child: Wrap(
              direction: Axis.horizontal,
              children: const [
                Text(
                  "800,000",
                  style: TextStyle(
                      color: Color(0xffff6006),
                      fontWeight: FontWeight.w500,
                      fontSize: 25),
                ),
                Text(
                  " đ",
                  style: TextStyle(
                      fontSize: 20,
                      color: Color(0xffff6006),
                      fontWeight: FontWeight.w500,
                      fontFeatures: [
                        FontFeature.superscripts(),
                      ]),
                ),
              ],
            ),
          )
        ],
      ),
    ]);
  }
}
