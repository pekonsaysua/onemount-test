import 'package:ecommerce/utils/theme.dart';
import 'package:flutter/material.dart';

class CustomChip extends StatefulWidget {
  final List<String> chipTextList;

  const CustomChip({super.key, required this.chipTextList});

  @override
  State<CustomChip> createState() => _CustomChipState();
}

class _CustomChipState extends State<CustomChip> {
  ValueNotifier<String> _selectedChip = ValueNotifier("");

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _selectedChip.value = widget.chipTextList[0];
  }

  void _onChipPressed(String chipText) {
    _selectedChip.value = chipText;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: ValueListenableBuilder(
        valueListenable: _selectedChip,
        builder: (context, value, child) {
          return ListView.builder(
            padding: const EdgeInsets.only(right: 10),
            scrollDirection: Axis.horizontal,
            itemCount: widget.chipTextList.length,
            itemBuilder: (context, index) {
              String chipText = widget.chipTextList[index];
              bool isSelected = value == chipText;

              return Padding(
                padding: const EdgeInsets.only(left: 10),
                child: ActionChip(
                  label: Text(
                    chipText,
                    style: TextStyle(
                        color: isSelected ? primaryColor : whiteColor),
                  ),
                  backgroundColor: isSelected
                      ? const Color(0xfffff2e2)
                      : const Color(0xffa40800),
                  onPressed: () {
                    _onChipPressed(chipText);
                  },
                ),
              );
            },
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _selectedChip.dispose();
    super.dispose();
  }
}
